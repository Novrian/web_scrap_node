
/*
Programmer: Nova Novriansyah
Last Update: Sep 8, 2018
Node ver: 8.11

*/


//Library included
var http = require("http");
var request = require('request');
var cheerio= require ('cheerio');
var rp = require('request-promise');
var fs = require('fs');

//initialisation

var main_url='https://www.bankmega.com';
var promo_url =main_url+ '/ajax.promolainnya.php';




//callback function for process detail page

function  process_page (error, response, body)  {



	if (!error && response.statusCode == 200 ) {


	 	var $ = cheerio.load(body);

    //get the current url and parameters
		var destUrl = this.uri.href;
		var token= destUrl.split('&');

	 	//getting sub category from url
	 	var subcat=decodeURI(token[1].split("=")[1]);
	 	var subcat_desc= sub_category_map[subcat];
	  console.log("visit sub category:  " + subcat_desc +' -->' + destUrl );

    //initialise json
	 	json[subcat_desc]=[];


    /*
    used for tracing
	 	//console.log("subcategory:"+  $(this).href);

	  //var len= $('#promolain li').find('img').html();
	 	//	console.log("len:"+  $('#promolain li a' ));
	  //var global.json2= {};
    */

   //get total page on sub category
		var page_text= $('.page_promo_lain').last().attr("title");
		var page = page_text.split(" ")[3];

		console.log("total page:" + page);

    //iterate to number of pages for every sub catalogue
	 	for (var i=1; i<=page;i++){
	 		var page_url= promo_url + '?product=0&subcat='+ subcat +'&page='+i ;
			console.log("page url 1:" + page_url);


      //request the url page
			var result=request( page_url, function (error, response, body){


					var $ = cheerio.load(body);


					//console.log("******	header:" +body);

					if (!error && response.statusCode == 200 ) {

              //loop for each promotions in subcategory to be visited
				      $('#promolain li a').each(function(i, elm) {
				   		      var url_detail = main_url+ "/" + $(this).attr('href');

						         console.log('url detail:'+ url_detail);


                    //visit every promo link
      							var options = {
      							    uri: url_detail,
      							    headers: {
      							       'Host': 'www.bankmega.com'
      							    },
      							    transform: function (body) {
      							        return cheerio.load(body);
      							    }
      							};

							      rp(options)
							      .then(function ($) {


							                    console.log('scrapping url : '+ url_detail);
              								  	/*

              								  	console.log("title :" , $('.titleinside').text().trim());
              								  	console.log("periode: " +  $('.periode b').text());

                									console.log("image: " + main_url +  $('.keteranganinside img').attr('src'));
                									console.log("area: " + $('.area').text());
                								 	*/

              								 	var json3= "{title:'"+ $('.titleinside').text().trim() + "', " + "imageUrl:'" + main_url +  $('.keteranganinside img').attr('src') + "', " + "area:'" + $('.area').text()+ "'+ ',url='"+ url_detail + "'}";
              									//console.log("json3" + json3);

              									var subcat_desc= sub_category_map[subcat];


              									json[subcat_desc].push(json3);

				                        //console.log("=====>json2:" + JSON.stringify(json));

								                var 	json_string= JSON.stringify(json,null,4);

              									//write tp file
              									fs.writeFile('solution.json', json_string, 'utf8', function() {

									              });



							      })
  							    .catch(function (err) {
  							        // Crawling failed or Cheerio choked...
  							        console.log('error: ', err.statusCode);
  							    });

						});

					}


			});
	 	}








	}



}

// main function



var sub_category_map={};
var  json = {}

console.log("Scrapping start");
request( promo_url, function  (error, response, body) {

			const $ = cheerio.load(body);

			var i=0;
			var title='';
			console.log("Sub Category Identified: " );
			$('#subcatpromo div img ').each(function(i, elm) {

					//console.log($(this).html());

					title=$(this).attr('title');

					i++;
					sub_category_map[i]=title;

					//	json[title]=[];

					console.log(i+ "," + sub_category_map[i]);
					//console.log("json:"+ JSON.stringify(json));

					var sub_cat_url= promo_url + '?product=0&subcat='+ i  ;

					var result=request( sub_cat_url, process_page);

					//console.log("======>result : " +  JSON.stringify(result));





			});





});
